package main

import (
	"flag"
	"github.com/matrix-org/gomatrix"
	"gopkg.in/yaml.v2"
	"io/ioutil"
	"log"
	"time"
)

type configFilter struct {
	Type string   `yaml:"type"`
	List []string `yaml:"list"`
}
type config struct {
	Homeserver string         `yaml:"homeserver"`
	Token      string         `yaml:"token"`
	Filter     configFilter   `yaml:"filter"`
	Invite     map[string]int `yaml:"invite"`
}

func main() {
	log.SetFlags(0)

	var configPath = flag.String("config", "config.yaml", "Path to the configuration file")
	var runOnce = flag.Bool("once", false, "Run only once")
	var period = flag.Int64("period", 60, "Time between two checks")
	var verbose = flag.Bool("v", false, "Verbose mode")
	flag.Parse()

	data, err := ioutil.ReadFile(*configPath)
	if err != nil {
		log.Fatal(err)
	}

	var cfg config
	err = yaml.Unmarshal(data, &cfg)
	if err != nil {
		log.Fatal(err)
	}

	log.Print("Matrix homeserver: ", cfg.Homeserver)
	mxClient, err := gomatrix.NewClient(
		cfg.Homeserver, "", cfg.Token,
	)
	if err != nil {
		log.Fatal(err)
	}

	processedRooms := make(map[string]bool)

	for {
		joinedRooms, err := mxClient.JoinedRooms()
		if err != nil {
			log.Fatal(err)
		}

		for _, joinedRoom := range joinedRooms.JoinedRooms {
			if *verbose {
				log.Print("==> ", joinedRoom)
			}

			// Handle filtering
			if cfg.Filter.Type == "blacklist" {
				for _, room := range cfg.Filter.List {
					if joinedRoom == room {
						if *verbose {
							log.Print("  ", joinedRoom, " is blacklisted")
						}
						continue
					}
				}
			} else if cfg.Filter.Type == "whitelist" {
				for _, room := range cfg.Filter.List {
					if joinedRoom != room {
						if *verbose {
							log.Print("  ", joinedRoom, " is not whitelisted")
						}
						continue
					}
				}
			} else {
				log.Fatal("Config.filter.type '" + cfg.Filter.Type + "' is not allowed.")
			}

			if _, found := processedRooms[joinedRoom]; !found {
				processedRooms[joinedRoom] = true

				log.Print("New room: ", joinedRoom)

				joinedMembers, err := mxClient.JoinedMembers(joinedRoom)
				if err != nil {
					if *runOnce {
						log.Fatal("Cannot request list of joined members: ", err)
					} else {
						log.Print("Cannot request list of joined members: ", err)
						continue
					}
				}

				for id, pl := range cfg.Invite {

					found = false
					for memberID := range joinedMembers.Joined {
						if memberID == id {
							found = true
							break
						}
					}

					if !found {
						log.Print("  Invite ", id, " to ", joinedRoom)

						_, err := mxClient.InviteUser(joinedRoom, &gomatrix.ReqInviteUser{
							UserID: id,
						})
						if err != nil {
							if *runOnce {
								log.Fatal("Cannot invite user: ", err)
							} else {
								log.Print("Cannot invite user: ", err)
								continue
							}
						}

						// Get current power level state of the room
						var powerlevels map[string]interface{}
						err = mxClient.StateEvent(joinedRoom, "m.room.power_levels", "", &powerlevels)
						if err != nil {
							if *runOnce {
								log.Fatal("Cannot get m.room.power_levels state event: ", err)
							} else {
								log.Print("Cannot get m.room.power_levels state event: ", err)
								continue
							}
						}

						log.Print("  Set power level of ", id, " to ", pl, " in ", joinedRoom)
						powerlevels["users"].(map[string]interface{})[id] = pl

						_, err = mxClient.SendStateEvent(joinedRoom, "m.room.power_levels", "", &powerlevels)
						if err != nil {
							if *runOnce {
								log.Fatal("Cannot set m.room.power_levels state event: ", err)
							} else {
								log.Print("Cannot set m.room.power_levels state event: ", err)
								continue
							}
						}
					}

				}
			}

		}

		if *runOnce {
			break
		}

		time.Sleep(time.Duration(*period) * time.Second)
	}
}
