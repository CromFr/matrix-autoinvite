# Synchronize room membership between two users

This service scans rooms joined by its Matrix user, and invites other users to the rooms.


# Build

```bash
go build
```

# Install
```bash
# Install files on your system
mkdir /opt/matrix-autoinvite
cp matrix-autoinvite config.example.yaml /opt/
cp matrix-autoinvite.service /etc/systemd/system/

# Create/Edit config.yaml
cp /opt/matrix-autoinvite/config{.example,}.yaml
nano /opt/matrix-autoinvite/config.yaml

# Start service
systemctl start matrix-autoinvite.service
```